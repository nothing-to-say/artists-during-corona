import React, {useState} from 'react';
import './main.scss';
import Entry from './Entry';
import User from './User';
import * as firebase from './Firebase';
import SignInScreen from './SignInScreen';
import {ContentEditor, serializeEditorValueToHtml} from './ContentEditor';
import {withRouter, Link} from 'react-router-dom';
import * as urls from './urls';
import Metadata from './Metadata';


/**
 * I'm hackily using this one class for both the "review mode" form
 * and the "submit mode" form. This would be better with inheritance.
 */
class Submit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      submitterName: "",
      submitterHref: "",
      tags: "",
      editableContent: null,
      isSignedIn: false,
      reviewMode: false,
      submitterUser: null,
      entryId: null,
    };

    if (this.props.match.params.id) {
      firebase.getEntry(this.props.match.params.id)
        .then(documentSnapshot => {
          const entry = documentSnapshot.data();
          this.setState({
            title: entry.title,
            submitterName: entry.submitter.name,
            submitterHref: entry.submitter.href,
            tags: entry.tags.join(','),
            editableContent: entry.editableContent,
            reviewMode: true,
            submitterUser: entry.submitter,
            entryId: entry.id,
            approved: entry.approved,
            rejected: entry.rejected,
            date: entry.date,
          });
        })
        .catch(e => {
          console.log(`error executing query: ${e}`);
          throw e;
        });
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.signOut = this.signOut.bind(this);
    this.hardDelete = this.hardDelete.bind(this);
  }

  handleSubmit(e) {
    if (this.state.reviewMode) {
      this.handleSubmitReviewMode(e);
      return;
    }

    const absoluteSubmitterHref = urls.makeAbsolute(this.state.submitterHref);
    const user = firebase.getSignedInUser(this.state.submitterName, absoluteSubmitterHref);

    // create new entry to submit
    const entry = new Entry(
      user,
      this.state.title,
      this.state.editableContent,
      null,
      this.state.tags.split(","),
      false,
      false,
      new Date(),
    );

    console.log(JSON.stringify(entry));

    // submit entry to db
    firebase.submitEntry(entry)
      .then((docRef) => console.log(`Made document with ID: ${docRef.id}`))
      .catch((e) => console.log(`Error adding document: ${e}`));

    e.preventDefault();
    this.props.history.push("/submitted");
  }

  handleSubmitReviewMode(e) {
    // signed in user
    const reviewer = firebase.getSignedInUser(null, null);
    if (!this.state.isSignedIn) alert("you are not signed in...");

    const renderedContent = serializeEditorValueToHtml(this.state.editableContent);

    const absoluteSubmitterHref = urls.makeAbsolute(this.state.submitterHref);
    const user = new User(
      this.state.submitterUser.id, this.state.submitterName, absoluteSubmitterHref);

    // create new entry to submit
    const entry = new Entry(
      user,
      this.state.title,
      this.state.editableContent,
      renderedContent,
      this.state.tags.split(","),
      this.state.approved,
      this.state.rejected,
      this.state.date,
      this.state.entryId,
    );

    // submit entry to db
    firebase.updateEntry(entry)
      .catch((e) => console.log(`Error adding document: ${e}`));

    e.preventDefault();
    this.props.history.push("/review");
  }

  hardDelete() {
    if (!window.confirm('Are you sure you want to permanently delete this?')) {
      return;
    }
    firebase.deleteEntry(this.state.entryId)
      .then(() => this.props.history.push("/review"))
      .catch((e) => console.log(`Error deleting document: ${e}`));
  }

  signOut() {
    firebase.signOut();
    this.props.history.push("/");
  }

  renderSubmitForm() {
    return (
      <div>
        <Metadata title="Artists During Corona" />
        <form className="Submit-Form" onSubmit={this.handleSubmit}>
          <label className="Submit-Form-Label">
            title:
            <input
              value={this.state.title}
              onChange={e => this.setState({title: e.target.value})}
            />
          </label>
          <label className="Submit-Form-Label">
            your name:
            <input
              value={this.state.submitterName}
              onChange={e => this.setState({submitterName: e.target.value})}
            />
          </label>
          <label className="Submit-Form-Label">
            your website:
            <input
              value={this.state.submitterHref}
              onChange={e => this.setState({submitterHref: e.target.value})}
            />
          </label>
          {(!this.props.match.params.id || this.state.editableContent) &&
            <ContentEditor
              value={this.state.editableContent}
              onChange={value => this.setState({editableContent: value})}
            />
          }
          {this.state.reviewMode &&
            <div>
              <label className="Submit-Form-Label">
                tags:
                <input
                  placeholder="music, visual art, etc..."
                  value={this.state.tags}
                  onChange={e => this.setState({tags: e.target.value})}
                />
              </label>
              <label className="Submit-Form-Label">
                approved:
                <input
                  type="checkbox"
                  checked={this.state.approved}
                  onChange={e => this.setState({approved: e.target.checked})}
                />
              </label>
              <label className="Submit-Form-Label">
                rejected:
                <input
                  type="checkbox"
                  checked={this.state.rejected}
                  onChange={e => this.setState({rejected: e.target.checked})}
                />
              </label>
              <input type="button" value="hard delete" onClick={this.hardDelete}/>
            </div>
          }
          <div className="Agree-and-Submit">
            <p>
  by clicking "submit" you agree to publish this submission, possibly with cosmetic edits made by the <i>nothing to say</i> editors, under a <a href="https://creativecommons.org/licenses/by-sa/4.0/">creative commons by-sa</a> license. all submissions published will publicly include your name and website link. your email address will not be made public.
            </p>
            <input type="submit" value={this.state.reviewMode ? "save" : "submit"} />
          </div>
        </form>
        <button onClick={ this.signOut }>
          sign out
        </button>
      </div>
    )
  }

  render() {
    const signUpForm = (
      <div>
        <p>
          Thank you for your interest in sharing your thoughts! If you haven't already, please check out <Link to="/about">the <i>about</i> page</Link> for information about the project and submission process.
        </p>
        <p>
        To submit, please sign in by email. We won't share this with anyone or put it on any mailing lists. No password is needed; we'll just send you a link to verify you own the address. If your website has an email address on it, please use that.
        </p>
        <SignInScreen
          handleAuthChange={signedIn => this.setState({isSignedIn: signedIn})}
        />
      </div>
    );

    return (
      <div className="Submit-Page">
        { this.state.isSignedIn ? this.renderSubmitForm() : signUpForm }
      </div>
    );
  }
}



export default withRouter(Submit);
