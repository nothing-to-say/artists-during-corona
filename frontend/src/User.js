class User {
  constructor(id, name, href) {
    this.id = id;
    this.name = name;
    this.href = href;
  }
}

export default User;
