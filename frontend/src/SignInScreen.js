import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';  // Required for side-effects
import React from 'react'
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';


class SignInScreen extends React.Component {
  constructor(props) {
    super(props);

    this.handleAuthChange = props.handleAuthChange;
  }

  // Listen to the Firebase Auth state and set the local state.
  componentDidMount() {
    this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
      (user) => this.handleAuthChange(!!user)
    );
  }
  
  // Make sure we un-register Firebase observers when the component unmounts.
  componentWillUnmount() {
    this.unregisterAuthObserver();
  }

  render() {
    return (
      <div>
        <StyledFirebaseAuth
          uiConfig={{
            signInOptions: [
              {
                provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
                signInMethod: firebase.auth.EmailAuthProvider.EMAIL_LINK_SIGN_IN_METHOD,
                emailLinkSignIn: function() {
                  return {
                    url: `${document.location.href.split(/[?#]/)[0]}`
                  };
                },
              }
            ],
          }}
          firebaseAuth={firebase.auth()}
        />
      </div>
    );
  }
}

export default SignInScreen;
