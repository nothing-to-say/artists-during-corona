class Entry {
  constructor(submitter, title, editableContent, renderedContent, tags, approved, rejected, date, id=null) {
    this.submitter = submitter;
    this.title = title;
    this.editableContent = editableContent;
    this.renderedContent = renderedContent;
    this.tags = tags;
    this.approved = approved;
    this.rejected = rejected;
    this.date = date;
    this.id = id;
  }
}

export default Entry;
