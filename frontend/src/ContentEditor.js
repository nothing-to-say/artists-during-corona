import React, { useCallback, useMemo, useState } from 'react'
import ReactDOMServer from 'react-dom/server';
import isHotkey from 'is-hotkey'
import { Editable, withReact, useSlate, Slate } from 'slate-react'
import { Editor, Transforms, Range, createEditor } from 'slate'
import { withHistory } from 'slate-history'
import isUrl from 'is-url'
import { Button, Icon, Toolbar } from './SlateComponents'
import * as urls from './urls';

/**
 * This is very lightly adapted from Slate's official repo examples,
 * specifically the rich text example and the links example.
 * 
 * https://github.com/ianstormtaylor/slate/tree/master/site/examples
 */

const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
}

const LIST_TYPES = ['numbered-list', 'bulleted-list']

function serializeEditorValueToHtml(el) {
  return ReactDOMServer.renderToStaticMarkup(
    <div>{editorValueToReactComponents(el)}</div>
  );
}

function editorValueToReactComponents(el) {
  if (Array.isArray(el)) {
    return el.map((subElement, idx) => editorValueToReactComponents(subElement));
  }
  switch (el.type) {
  case 'paragraph':
    return <p>{editorValueToReactComponents(el.children)}</p>;
  case 'list-item':
    return <li>{editorValueToReactComponents(el.children)}</li>;
  case 'heading':
    return <h3>{editorValueToReactComponents(el.children)}</h3>;
  case 'block-quote':
    return <blockquote>{editorValueToReactComponents(el.children)}</blockquote>;
  case 'numbered-list':
    return <ol>{editorValueToReactComponents(el.children)}</ol>;
  case 'bulleted-list':
    return <ul>{editorValueToReactComponents(el.children)}</ul>;
  case 'bold':
    return <strong>{editorValueToReactComponents(el.children)}</strong>;
  case 'italic':
    return <em>{editorValueToReactComponents(el.children)}</em>;
  case 'link':
    return <a href={el.url}>{editorValueToReactComponents(el.children)}</a>;
  default:
    return el.text;
  }
}

const ContentEditor = (props) => {
  const [value, setValue] = useState(props.value ? props.value : initialValue)
  const renderElement = useCallback(props => <Element {...props} />, [])
  const renderLeaf = useCallback(props => <Leaf {...props} />, [])
  const editor = useMemo(() => withLinks(withHistory(withReact(createEditor()))), [])
  
  return (
      <Slate editor={editor} value={value} onChange={value => {
        setValue(value);
        props.onChange(value);
      }}>
      <Toolbar>
        <LinkButton />
        <MarkButton format="bold" icon="format_bold" />
        <MarkButton format="italic" icon="format_italic" />
        <BlockButton format="heading" icon="looks_one" />
        <BlockButton format="block-quote" icon="format_quote" />
        <BlockButton format="numbered-list" icon="format_list_numbered" />
        <BlockButton format="bulleted-list" icon="format_list_bulleted" />
      </Toolbar>
      <Editable
        className="Editable-Body"
        renderElement={renderElement}
        renderLeaf={renderLeaf}
        placeholder="what do you want people to know?"
        spellCheck
        autoFocus
        onKeyDown={event => {
          for (const hotkey in HOTKEYS) {
            if (isHotkey(hotkey, event)) {
              event.preventDefault()
              const mark = HOTKEYS[hotkey]
              toggleMark(editor, mark)
            }
          }
        }}
      />
    </Slate>
  )
}

const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format)
  const isList = LIST_TYPES.includes(format)

  Transforms.unwrapNodes(editor, {
    match: n => LIST_TYPES.includes(n.type),
    split: true,
  })

  Transforms.setNodes(editor, {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  })

  if (!isActive && isList) {
    const block = { type: format, children: [] }
    Transforms.wrapNodes(editor, block)
  }
}

const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format)

  if (isActive) {
    Editor.removeMark(editor, format)
  } else {
    Editor.addMark(editor, format, true)
  }
}

const isBlockActive = (editor, format) => {
  const [match] = Editor.nodes(editor, {
    match: n => n.type === format,
  })

  return !!match
}

const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor)
  return marks ? marks[format] === true : false
}

const Element = ({ attributes, children, element }) => {
  switch (element.type) {
    case 'block-quote':
      return <blockquote {...attributes}>{children}</blockquote>
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>
    case 'heading':
      return <h3 {...attributes}>{children}</h3>
    case 'list-item':
      return <li {...attributes}>{children}</li>
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>
    case 'link':
      return (
        <a {...attributes} href={element.url}>
          {children}
        </a>
      )
    default:
      return <p {...attributes}>{children}</p>
  }
}

const Leaf = ({ attributes, children, leaf }) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>
  }

  if (leaf.italic) {
    children = <em>{children}</em>
  }

  return <span {...attributes}>{children}</span>
}

const LinkButton = () => {
  const editor = useSlate()
  return (
    <Button
      active={isLinkActive(editor)}
      onMouseDown={event => {
        event.preventDefault()
        const url = window.prompt('Enter the URL of the link:')
        if (!url) return
        insertLink(editor, urls.makeAbsolute(url))
      }}
    >
      <Icon>link</Icon>
    </Button>
  )
}

const BlockButton = ({ format, icon }) => {
  const editor = useSlate()
  return (
    <Button
      active={isBlockActive(editor, format)}
      onMouseDown={event => {
        event.preventDefault()
        toggleBlock(editor, format)
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  )
}

const MarkButton = ({ format, icon }) => {
  const editor = useSlate()
  return (
    <Button
      active={isMarkActive(editor, format)}
      onMouseDown={event => {
        event.preventDefault()
        toggleMark(editor, format)
      }}
    >
      <Icon>{icon}</Icon>
    </Button>
  )
}

const withLinks = editor => {
  const { insertData, insertText, isInline } = editor

  editor.isInline = element => {
    return element.type === 'link' ? true : isInline(element)
  }

  editor.insertText = text => {
    if (text && isUrl(text)) {
      wrapLink(editor, text)
    } else {
      insertText(text)
    }
  }

  editor.insertData = data => {
    const text = data.getData('text/plain')

    if (text && isUrl(text)) {
      wrapLink(editor, text)
    } else {
      insertData(data)
    }
  }

  return editor
}

const insertLink = (editor, url) => {
  if (editor.selection) {
    wrapLink(editor, url)
  }
}

const isLinkActive = editor => {
  const [link] = Editor.nodes(editor, { match: n => n.type === 'link' })
  return !!link
}

const unwrapLink = editor => {
  Transforms.unwrapNodes(editor, { match: n => n.type === 'link' })
}

const wrapLink = (editor, url) => {
  if (isLinkActive(editor)) {
    unwrapLink(editor)
  }

  const { selection } = editor
  const isCollapsed = selection && Range.isCollapsed(selection)
  const link = {
    type: 'link',
    url,
    children: isCollapsed ? [{ text: url }] : [],
  }

  if (isCollapsed) {
    Transforms.insertNodes(editor, link)
  } else {
    Transforms.wrapNodes(editor, link, { split: true })
    Transforms.collapse(editor, { edge: 'end' })
  }
}

const initialValue = [{type:"paragraph",children:[{text:""}]}]

export {
  ContentEditor, serializeEditorValueToHtml
};
