import React from 'react';
import EntryFeed from './EntryFeed';
import Metadata from './Metadata';

class Home extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return(
      <div>
        <Metadata title="Artists During Corona" />
        <div className="Project-Summary">
        as the world is turned upside-down, creators everywhere are responding in extraordinary ways. mutual aid is rapidly organizing; performances are moving online; institutions are on the verge of collapse; social media looms larger than ever before. this submission-based mini-blogging project is being organized by <a href="//nothing-to-say.org">nothing to say</a> with the hope it can serve as a living, contemporaneous account of some small part of how art changed in 2020.
        </div>
        <div className="Page-Title">
          <h1>latest entries</h1>
        </div>
        <EntryFeed/>
      </div>
    );
  }
}

export default Home;
