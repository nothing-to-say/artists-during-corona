import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/analytics';
import 'firebase/firestore';  // Required for side-effects
import Entry from './Entry';
import User from './User';


const firebaseConfig = {
  // this is all public-safe data btw...
  apiKey: "AIzaSyD2a_oCib7DTIVp63xfE0w6QV3oAXJzpf0",
  authDomain: "artists-during-corona.firebaseapp.com",
  databaseURL: "https://artists-during-corona.firebaseio.com",
  projectId: "artists-during-corona",
  storageBucket: "artists-during-corona.appspot.com",
  messagingSenderId: "224189210665",
  appId: "1:224189210665:web:f650a1b46f8791617ebc38",
  measurementId: "G-K0N270347N"
};

firebase.initializeApp(firebaseConfig);
firebase.analytics();
const db = firebase.firestore();

function userToObject(user) {
  return {
    id: user.id,
    name: user.name,
    href: user.href
  };
}

function objectToUser(user) {
  return new User(user.id, user.name, user.href);
}

const entryConverter = {
  toFirestore: (entry) => {
    return {
      submitter: userToObject(entry.submitter),
      title: entry.title,
      editableContent: entry.editableContent,
      renderedContent: entry.renderedContent,
      tags: entry.tags,
      approved: entry.approved,
      rejected: entry.rejected,
      date: firebase.firestore.Timestamp.fromDate(entry.date)
    };
  },
  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    return new Entry(
      objectToUser(data.submitter),
      data.title,
      data.editableContent,
      data.renderedContent,
      data.tags,
      data.approved,
      data.rejected,
      data.date.toDate(),
      snapshot.id
    );
  }
};


/**
 * Submit an entry to the database.
 *
 * Validation occurs in firebase, so we don't need to validate here.
 * Example:
 *
 * submitEntry(someEntry)
 *     .then((docRef) => console.log(`Made document with ID: ${docRef}`))
 *     .catch((e) => console.log(`Error adding document: ${e}`));
 */
function submitEntry(entry) {
  return db.collection("entries").withConverter(entryConverter).add(entry);
}

function updateEntry(entry) {
  return db.collection("entries").withConverter(entryConverter).doc(entry.id).set(entry);
}

/**
 * query object model: {
 *     limit: Number,  // Required
 *     startAfter: DocumentSnapshot,  // Optional
 *     tag: String,  // Optional
 *     text: String,  // Optional
 *     approved: Boolean // Optional, defaults to true
 * }
 *
 * Returns a promise containing a firebase QuerySnapshot
 * See https://firebase.google.com/docs/reference/js/firebase.firestore.QuerySnapshot
 *
 *     search({limit: 2})
 *         .then(querySnapshot => {
 *             console.log(`got ${querySnapshot.size} results`);
 *             querySnapshot.forEach(doc => {
 *                 console.log(`${doc.id} -> ${JSON.stringify(doc.data(), null, 2)}`);
 *             });
 *         })
 *         .catch(e => {
 *             console.log(`error executing query: ${e}`);
 *         });
 *
 * To paginate, pass the last result document of the previous search into `startAfter`
 */
function search(query) {
  // relevant docs https://firebase.google.com/docs/firestore/query-data/queries
  let firebaseQuery = db.collection("entries").withConverter(entryConverter)
      .orderBy('date', 'desc')
      .where('approved', '==', query.hasOwnProperty('approved') ? query.approved : true)
      .limit(query.limit);
  if (query.hasOwnProperty('startAfter') && query.startAfter) {
    firebaseQuery = firebaseQuery.startAfter(query.startAfter.date);
  }
  if (query.hasOwnProperty('tag') && query.tag) {
    firebaseQuery = firebaseQuery.where('tags', 'array-contains', query.tag);
  }
  if (query.hasOwnProperty('text') && query.text) {
    // Firebase doesn't support full text search, so we'd have to build an algolia
    // integration or something to index. it can be done, but not right now.
    console.warn('full text search is not implemented yet');
  }
  return firebaseQuery.get();
}

function getEntry(id) {
  return db.collection("entries").withConverter(entryConverter).doc(id).get();
}

function deleteEntry(id) {
  return db.collection("entries").withConverter(entryConverter).doc(id).delete();
}

function getSignedInUser(displayName, href) {
  const currentUser = firebase.auth().currentUser;
  if (currentUser === null) {
    return null;
  }
  return new User(currentUser.uid, displayName, href);
}

function signOut() {
  return firebase.auth().signOut();
}

export {
  getEntry,
  submitEntry,
  updateEntry,
  deleteEntry,
  search,  
  getSignedInUser,
  signOut,
};
