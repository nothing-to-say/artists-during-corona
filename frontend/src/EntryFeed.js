import React from 'react';
import Entry from './Entry';
import InfiniteScroll from 'react-infinite-scroller';
import EntryComponent from './EntryComponent';
import * as firebase from './Firebase';

class EntryFeed extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      entries: [],
      hasMoreItems: true,
      queryPageSize: 4,
      queryStartAfter: null,
      queryTag: null,
      queryText: null,
      queryApproved: true,
    };
  }
  
  deriveQueryFromState() {
    return {
      limit: this.state.queryPageSize,
      startAfter: this.state.queryStartAfter,
      tag: this.state.queryTags,
      text: this.state.queryText,
      approved: this.state.queryApproved
    };
  }
  
  loadItems(page) {
    const loadedEntriesLen = this.state.entries.length;
    const lastEntry = loadedEntriesLen ? this.state.entries[loadedEntriesLen - 1] : null;
    const query = firebase.search(this.deriveQueryFromState())
      .then(querySnapshot => {
        if (querySnapshot.empty) {
          this.setState({hasMoreItems: false});
          return;
        }
        let entries = [...this.state.entries];
        querySnapshot.forEach(doc => entries.push(doc.data()));
        this.setState({
          entries: entries,
          queryStartAfter: entries[entries.length - 1]
        });
      })
      .catch(e => {
        console.log(`error executing query: ${e}`);
        throw e;
      });
    
  }
  
  render() {
    const loader = <div className="loader">Loading ...</div>;
    const entryComponents = this.state.entries.map((entry, idx) => {
      return (
        <EntryComponent
          key={idx} 
          id={entry.id}
          submitter={entry.submitter}
          title={entry.title}
          renderedContent={entry.renderedContent}
          tags={entry.tags}
          date={entry.date}
        />
      );
    });
    
    return (
      <InfiniteScroll
         loadMore={this.loadItems.bind(this)}
         hasMore={this.state.hasMoreItems}
         loader={loader}>

        <div className="Entry-List">
          {entryComponents}
        </div>

      </InfiniteScroll>
    );
  }
}


export default EntryFeed;
