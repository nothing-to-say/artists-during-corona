function makeAbsolute(url) {
  const sanitized = url.trim();
  if (!sanitized || sanitized.startsWith("//") ||
      sanitized.startsWith("http") || sanitized.startsWith("mailto:")) {
    return sanitized;
  }
  return "//" + sanitized;
}

export {
  makeAbsolute
}
