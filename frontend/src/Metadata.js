import React from 'react';
import {Helmet} from 'react-helmet';


export default (props) => {
  return (
      <Helmet>
        <title>{props.title}</title>
        <meta property="og:title" content={props.title} />
      </Helmet>
  );
}
