import React, {useState} from 'react';
import {useParams, withRouter, Link} from 'react-router-dom';
import * as firebase from './Firebase';
import EntryComponent from './EntryComponent';
import Metadata from './Metadata';

const EntryPermalink = (props) => {
  const { id } = useParams();
  const [entry, setEntry] = useState(null);
  
  if (!entry) {
    firebase.getEntry(id)
    .then(documentSnapshot => {
      setEntry(documentSnapshot.data());
    })
    .catch(e => {
      console.log(`error executing query: ${e}`);
      throw e;
    });
  }

  if (entry) {
    window.setTimeout(() => window.isPageReady = 1, 1000);
    return (
      <div>
        <Metadata title={entry.title} />
        <EntryComponent
          id={entry.id}
          submitter={entry.submitter}
          title={entry.title}
          renderedContent={entry.renderedContent}
          tags={entry.tags}
          date={entry.date}
        />
        <Link className="Read-More-Link" to="/">read more</Link>
      </div>
    );
  }
  return (
    <p> loading... </p>
  );
};

export default withRouter(EntryPermalink);
