import ChromeRender from 'chrome-render';
import fs from 'fs';
import firebase from 'firebase';
import path from 'path';

const firebaseConfig = {
  // this is all public-safe data btw...
  apiKey: "AIzaSyD2a_oCib7DTIVp63xfE0w6QV3oAXJzpf0",
  authDomain: "artists-during-corona.firebaseapp.com",
  databaseURL: "https://artists-during-corona.firebaseio.com",
  projectId: "artists-during-corona",
  storageBucket: "artists-during-corona.appspot.com",
  messagingSenderId: "224189210665",
  appId: "1:224189210665:web:f650a1b46f8791617ebc38"
};

const app = firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

function search() {
  let firebaseQuery = db.collection("entries")
      .orderBy('date', 'desc')
      .where('approved', '==', true)
      .limit(10000);
  return firebaseQuery.get();
}


const outDir = path.resolve() + '/build/entry';
if (!fs.existsSync(outDir)) {
  fs.mkdirSync(outDir);
}

void async function() {
  const chromeRender = await ChromeRender.new();
  const querySnapshot = await search();
  let ids = [];
  querySnapshot.forEach(doc => ids.push(doc.id));
  
  for (const id of ids) {
    console.log(`rendering ${id}`);
    const htmlString = await chromeRender.render({
      url: `http://localhost:3000/entry/${id}`,
      useReady: true,
    });
    fs.writeFileSync(`${outDir}/${id}`, htmlString);
  };

  process.exit();
}();


