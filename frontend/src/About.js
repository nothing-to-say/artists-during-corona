import React from 'react';
import Metadata from './Metadata';

const About = (props) => {
  return(
    <div>
      <Metadata title="Artists During Corona" />
      <div class="Page-Title">
        <h1>about</h1>
      </div>
      <p>
        as the world is turned upside-down, creators everywhere are responding in extraordinary ways. mutual aid is rapidly organizing; performances are moving online; institutions are on the verge of collapse; social media looms larger than ever before. this submission-based mini-blogging project is being organized by <a href="//nothing-to-say.org">nothing to say</a> with the hope it can serve as a living, contemporaneous account of some small part of how art changed in 2020. we're collecting:
      </p>
      <ul>
        <li>personal stories about and reflections on how the pandemic has changed the lives and work of creators</li>
        <li>opinions on things creators and cultural institutions should be thinking about or doing right now</li>
        <li>journalistic descriptions and references to individual and collective actions people and institutions are taking to support one another</li>
      </ul>
      <p>
        <p>
          please note that while many entries may document grassroots-organized mutual aid initiatives, this is <i>not</i> primarily a resource aggregator. we recommend visiting the <a href="https://covid19freelanceartistresource.wordpress.com/">covid-19 & freelance artists portal</a> for this.
        </p>
      </p>

      <h2>the submission process</h2>
      <p>
        we want to hear from you. to submit to the project, head over to the <a href="/submit">submission page</a>. the <i>nothing to say</i> editors will review your submission. we may make minor edits to better suit publication, but if we need any significant edits we'll get in touch with you. once approved, we'll publish your entry and let you know.
      </p>
      <p>
        to be accepted, submissions should:
      </p>
      <ul>
        <li>be relevant</li>
        <li>not be advertisements; especially when describing actions taken by institutions, we'll be skeptical of for-profit companies</li>
        <li>not be simply promoting your work</li>
        <li>cite sources when available</li>
        <li>but not <i>just</i> be a link to a webpage (resources, news articles, blogs, etc); links are great but please include a brief summary in your own words of what you're documenting</li>
      </ul>

      <h2>about nothing to say</h2>
      <p>
        <a href="//nothing-to-say.org">nothing to say</a> is a 501(c)3 non-profit arts journal trying to make a place where emerging and underrepresented creators can share their thoughts. we publish medium/long-form essays and <a href="https://nothing-to-say.org/something-to-say">anonymous art reviews</a> online and in the occasional zine.
      </p>

      <h2>about this website</h2>
      <p>
        this website is open source and <a href="https://www.gnu.org/philosophy/free-sw.html">Free</a>; its source code can be found <a href="https://gitlab.com/nothing-to-say/artists-during-corona">here</a>. except where otherwise noted, all content is published under the <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">creative commons by-sa 4.0 license</a> under the authors' names, meaning you are invited to freely remix and share it.
      </p>
      <p>
        we don't run any advertisements and we aren't soliciting any donations, but we do collect anonymized usage statistics to see how we're doing. to opt out, please feel free to use an ad-blocker (we don't use it for ads, but blockers will stop it all the same).
      </p>
      <p>
        to report bugs or ask questions, please reach out to us at <a href="mailto:editors@nothing-to-say.org">editors@nothing-to-say.org</a>.
      </p>
      <p>
        special thanks to <a href="https://soundcloud.com/slimechurch">coco walsh</a> for help building this!
      </p>
    </div>
  );
};

export default About;
