import React from 'react';

const SubmissionSuccess = (props) => {
  return (
    <div className="Submission-Success">
      <p>
        Thank you for your submission! We try to review all submissions within a couple days of receipt, so if you don't hear back in a week feel free to <a href="mailto:editors@nothing-to-say.org">get in touch</a>. We'll email you when your submission is published or rejected. If you'd like to <a href="/submit">submit a few more entries</a> you're welcome to. Stay safe out there! &lt;3
      </p>
    </div>
  );
};

export default SubmissionSuccess;
