import React from 'react';
import * as firebase from './Firebase';
import {
  Switch,
  Route,
  Link,
  withRouter
} from "react-router-dom";
import Submit from './Submit';


class Review extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      submissions: [],
      rejected: [],
    };
  }
  
  componentDidMount() {
    firebase.search({
      limit: 50,
      approved: false
    }).then(querySnapshot => {
      let submissions = [];
      let rejected = [];
      querySnapshot.forEach(doc => {
        const submission = doc.data();
        if (submission.rejected) {
          rejected.push(submission);
        } else {
          submissions.push(submission);
        }
      });
      this.setState({
        submissions: submissions,
        rejected: rejected,
      });
    }).catch(e => {
      console.log(`error executing query: ${e}`);
    });
  }
  
  renderSubmissionSummary(entry, idx) {
    return (
      <li key={idx}>
        <Link to={{pathname: "/review/submission/" + entry.id}}>
          <i>{entry.title}</i> submitted by {entry.submitter.name} on {entry.date.toLocaleDateString()}
        </Link>
      </li>
    );
  }
  
  render() {
    return(
      <div>
      <h1>Review</h1>
      <ul>
        {this.state.submissions.map((entry, idx) => (
          this.renderSubmissionSummary(entry, idx)
        ))}
      </ul>
      <h2>Rejected</h2>
      <ul>
        {this.state.rejected.map((entry, idx) => (
          this.renderSubmissionSummary(entry, idx)
        ))}
      </ul>
      </div>
    );
  }
}

export default withRouter(Review);
