import React from 'react';
import Entry from './Entry';
import * as xss from 'xss';
import './main.scss';
import {Link, withRouter} from 'react-router-dom';


const EntryComponent = (props) => {
  const submitterInfo = props.submitter.href ? (
    <a className="User-Link" href={props.submitter.href}>{props.submitter.name}</a>
  ) : (
    <span>{props.submitter.name}</span>
  );
  return (
    <div className="Entry">
      <div className="Entry-Heading">
        <Link to={{pathname: '/entry/' + props.id}}>
          <h2 className="Entry-Title">{props.title}</h2>
        </Link>
        <p className="Byline">
          from {submitterInfo}, <span className="Entry-Date">{props.date.toLocaleDateString()}</span>
        </p>
      </div>
      <div className="Entry-Content" dangerouslySetInnerHTML={{__html: xss(props.renderedContent)}}>
      </div>
    </div>
  );
};

export default withRouter(EntryComponent);
