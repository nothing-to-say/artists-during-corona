import React from 'react';
import logo from './logo.svg';
import './main.scss';
import Submit from './Submit';
import About from './About';
import Review from './Review';
import Home from './Home';
import SubmissionSuccess from './SubmissionSuccess';
import EntryPermalink from './EntryPermalink';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

const pages = {
  home: "HOME",
  submit: "SUBMIT",
  about: "ABOUT",
};

class App extends React.Component {
  render() {
    return (
        <Router basename={process.env.PUBLIC_URL}>
        <div className="App">
          <div className="Header">
            <div className="Header-Container">
              <Link to="/" className="Header-Title">artists during corona</Link>
              <div className="Header-Nav">
                <Link to="/submit" className="Header-Item">submit</Link>
                  <a href="//nothing-to-say.org" className="Header-Item" target="_blank">nothing to say</a>
                <Link to="/about" className="Header-Item">about</Link>
              </div>
            </div>
          </div>

          <div className="content-wrapper">
            <Switch>
              <Route path="/submit">
                <Submit />
              </Route>
              <Route path="/submitted">
                <SubmissionSuccess />
              </Route>
              <Route path="/about">
                <About />
              </Route>
              <Route path="/review/submission/:id">
                <Submit />
              </Route>
              <Route path="/entry/:id">
                <EntryPermalink />
              </Route>
              <Route path="/review">
                <Review />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </div>
      </Router>
    );
  }
}

export default App;
